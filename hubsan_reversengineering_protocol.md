# Reverse engineering Hubsan X4 Copter #

See

1. [Part 1 - Hacking the controller](http://www.jimhung.co.uk/?p=1349 'JimHung's protocol - Part 1')
2. [Part 2 - Reversing the protocol](http://www.jimhung.co.uk/?p=1424 'JimHung's protocol - Part 2')
3. [Part 3 - The protocol definition](http://www.jimhung.co.uk/?p=1638 'JimHung's protocol - Part 3')
4. [Part 4 - Putting it to use](http://www.jimhung.co.uk/?p=1704 'JimHung's protocol - Part 4')
5. [Bonus Hack - Hacking the controller](http://www.jimhung.co.uk/?p=1769 'JimHung's protocol - Bonus')

## JimHung's protocol ##
see "Hubsan X4 Protocol Specification RFC v.1.0.md" (by JimHung)

## The Controller ##
* RF-transmitter: AMIC A7105 2.4Ghz Transceiver (can be controlled over 3-wire and 4-wire SPI)
* Microcontroller Control Unit (MCU): STMicroelectronics STM8S003-series 8-bit processor (supports UART, SPI and I2C)
* Available debug pins (see Part 1)

## Communication ##
see [JimHung.com_Hubsan Control Protocol Decoding.xlsx](https://drive.google.com/file/d/0BxbgjO2wxb0iWVMzc0NXSXBkMHc/edit) referred to as protocol_decoding_sheet for list of exchange information between the controller and the drone during flight.

### Transceiver setup ###
* Transceiver: A7105
* used pins: gio1, gnd (both), sdioa, sck, sds. See [A7105](http://www.electrodragon.com/w/A7105 'A7105 Pin definition') and [SPI](https://www.arduino.cc/en/Reference/SPI 'SPI Pin definition')

gio1 -> SPI (MISO)

sdio -> 10 kOhm ->SPI (MOSI)

sck -> SPI (SCK)

scs -> 4

### Description of communication setup ###
All of this is detailed in JimHung's protocol.
1. Binding Choregraphy

* Discovery
* Handshake 1
* Session ID
* Handshake 2
* Tx/Rx Power
* Handshake 3

2. Reversing the checksum algorithm 
cheksum = 256 - ([sum of packet bytes 1-15] % 256)

3. Flight control data

* Throttle: byte 3. From 0 to 100 %: from \x00 (0) to  \xFF (255) protocol_decoding_sheet rows  390-850

For all other categories is Throttle ~50% (\x80)

* Yaw: byte 5
  LEFT from center to 100%: \x80 (128) - \x44 (68) protocol_decoding_sheet rows 1250-1420
  RIGHT from center to 100%:  \x80 (128) - \xBF (191) protocol_decoding_sheet rows 1562-1719
* Pitch: byte 7
  FORWARD from center to 100%: \x80 (128) - \x41 (65) protocol_decoding_sheet rows 2344 -2480
  BACKWARD from center to 100%: \x80 (128) - \xBF (191) protocol_decoding_sheet rows 2616-2777
* Roll: byte 9
  LEFT from center to 100%: \x80 (128) - \xBF (191) protocol_decoding_sheet rows 2923-3068
  RIGHT from center to 100%: \x80 (128) - \x41 (65) protocol_decoding_sheet rows 3279-3392

## Using JimHung's programs ##
* Download all files from [JimHung's Git](https://github.com/NotionalLabs/libHubsan 'JimHung's Git').
* Add librairies folders (lib*) to Arduino's librairies.
* Download Arduino's package for managing Arduino Due, and upload Hubsan_H107L_Controller.ino via the Programming usb port on the Due.
* Start pyHubsan.py through command window (be sure to have python modules pygame, pyserial, configparser, and sys installed)
* Enter which port your Due is connected to in the command window
* Start Hubsan. A window pops up, be sure to have this window selected to control the drone.

## Implementing solution in ROS ##
to come