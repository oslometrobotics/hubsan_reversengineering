# Log #

This is the log file of the progress made in reverse engineering Hubsan drone, and implementing the solution in ROS.

21.02.17

* soldering of A7105 transceiver chip. (see [Part 4 - Putting it to use](http://www.jimhung.co.uk/?p=1704 'jimHung's protocol - Part 4')) Needed pins are gio1, gnd (both), sdio, sck and sds (see [A7105 Pin definitions](http://www.electrodragon.com/w/A7105 'Pin definition')) but all can be solded.
* implementation of JimHung's solution (python/arduino program to control hubsan drone through keyboard)

15.02.17 - Written readme and uploaded files:

* this readme.md
* hubsan_reversengineering_protocol.md (unfinished)
* Hubsan_X4_Protocol_Specification_RFC_v1.0.md