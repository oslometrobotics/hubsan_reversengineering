# README #
This readme will be fully edited soon.
The informations and techniques in this folder are a summary of information largely found at

1. [Part 1 - Hacking the controller](http://www.jimhung.co.uk/?p=1349 'Part 1')
2. [Part 2 - Reversing the protocol](http://www.jimhung.co.uk/?p=1424 'Part 2')
3. [Part 3 - The protocol definition](http://www.jimhung.co.uk/?p=1638 'Part 3')
4. [Part 4 - Putting it to use](http://www.jimhung.co.uk/?p=1704 'Part 4')

### What is this repository for? ###

* General technical information about hubsan drones
* Techniques for reverse engineering of hubsan drones
* Ultimately how-to instructions to programm and control a hubsan drone from a PC
* Ultimately how-to instructions to programm and control a hubsan drone from ROS

### Log ###

See log fil

### People currently working on this matter ###

* Robin Bjørkli
* Christophe Plaissy